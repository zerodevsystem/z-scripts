#!/bin/bash

sudo pacman -Sy
sudo pacman -S xorg lightdm lightdm-slick-greater dmenu lxappearance nitrogen archlinux-wallpaper picom firefox nano

sudo systenctl enable lightdm
sudo nano /etc/lightdm/lightdm.conf
#uncoment line: greater-seseion=example-gtk-gnome
#change to: greater-seseion=ligthtdm-slick-greater

#uncoment line: display-setup-script=xrand
#change to: display-setup-script=xrand --output Virtual-1 --mode 1920x1080

git clone https://aur.archlinux.org/paru-bin
cd paru-bin
makepkg -si

#paru -S timeshift timeshift-autosnap
paru -S zrand lightdm-settings

pacman -S grub-btrfs arc-gtk-theme papirus-icon-theme

sudo systemctl enable --now zrand
