#!/bin/bash

# Define a time zone
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

# Sincroniza o relógio do harware
hwclock --systohc

# Descomenta a linha pt_BR.UTF-8
sed -i '393s/.//' /etc/locale.gen
locale-gen
echo "LANG=pt_BR.UTF-8" >> /etc/locale.conf
echo "KEYMAP=br-abnt2" >> /etc/vconsole.conf
echo "ideapad-l340" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 ideapad-l340.localdomain ideapad-l340" >> /etc/hosts

# Você pode incluir os pacotes que achar necessário. Optamos por não instalar o Xorg porque o faremos por meio de script. Em caso de instalação em Desktop ou Máquina virtual pode retirar o pacote tlp da lista.

pacman -S noconfirm grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font

# pacman -S --noconfirm xf86-video-amdgpu
pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp 
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m -g users -G wheel -s /bin/bash zerocopia
usermod -aG libvirt zerocopia

echo "zerocopia ALL=(ALL) ALL" >> /etc/sudoers.d/zerocopia

echo "Informe a senha do root"
passwd root
echo "Informe a senha zerocopia"
passwd zerocopia

printf "\e[1;32mFim! Digite exit; umount -a e reboot.\e[0m"




