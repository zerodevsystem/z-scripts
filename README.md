# Zero Arch Install

Tutorial de instalação do Arch Linux e dos ambientes de desktop Gnome, KDE, XFCE e I3W.

## Getting started

Inspirado nos scripts de [Ermanno Ferrari](https://gitlab.com/eflinux/arch-basic).

## Description
O Arch Linux é uma distribuição simples, leve, flexível, rolling release e bleeding edge. A instalação mínima é bastante rápida, mas sem interface gráfica (GUI). Mas não se preocupe, neste tutorial aprenderemos a instalar o Arch e os ambientes de desktop Gnome, KDE, XFCE e I3W. Explore, estude, faça. Seja bem-vindo ao mundo Arch Linux.

## Preparação
A seguir aprenderemos a fazer o download da imagem mais atual do Arch Linux, verificar sua integridade, criar um pendrive _bootável_, desabilitar o _secure_ _boot_ na _BIOS_ para, então, começarmos a instalação. Siga cada um dos passos abaixo com atenção, mas antes, verifique se o seu wardware possui os requisitos mínimos necessários.

### Requisitos de Hardware
```
Processador: 64 bit (preferncialmente)
Memória: no mínimo 1 GB de RAM | recomendado 2 GB de RAM
Disco: mínimo 18 GB livre | Recomendado 30 GB livre (com GUI)
Pendrive de boot: mínimo de 4 GB livre
```
### 1. Download
Uma das maneiras mais tradicionais de instalar o Arch é fazendo o download do arquivo diretamente do site do [projeto](https://archlinux.org/download/). Neste tutorial optamos por fazê-lo do servidor do [Arch-Linux.br.org](archlinux-br.org). Fique atendo, o arquivo tem tem **812 MB**, a versão _Current Release_ é o **2022.02.01** e o _Kernel_ é o **5.16.4**. Além dessas informações, nas páginas do projeto você encontrará um guia de instalação, manual de comandos, dicas e tudo o que precisa para começar a utilizar o Arch Linux.

### 2. Verifique a integridade
Para verificar a integridade do arquivo baixado utilize esta assinatura [PGP Pública](https://archlinux.org/iso/2022.02.01/archlinux-2022.02.01-x86_64.iso.sig). Baixe o arquivo; abra o terminal do linux; navegue até o diretório que contiver o arquivo _.sig_ baixado e execute o comando:
```
gpg --keyserver-options auto-key-retrieve --verify archlinux-2022.02.01-x86_64.iso.sig
```
Caso utilize o Sistema Operacional Windows, a ferramenta [Gpg4win](https://www.gpg4win.org/) pode te ajudar. Baixe e instale o utilitário de linha de comando _CertUtil_, depois sigas as etapas descritas [aqui](https://antoniomedeiros.dev/blog/2018/10/05/verificacao-de-integridade-e-autenticidade-com-o-gpg4win/).

### 3. Crie um pendrive de boot
Existem diversas meios de criarmos uma mídia de instalação. O mais comuns é utilizando um pendrive [1]. Exitem diveros programas para isso. Um dos mais famosos é o [Balena Etcher](https://www.balena.io/etcher/). Sugerimos sua utilização, caso seu SO seja o Windows. Neste tutorial utilizaremos o comando [_dd_]. Abra o terminal do Linux, digite o comando _lsblk_ para identificar o pendrive [sdb1, sdb2, sdc, etc.] e depois execute o comando abaixo com as devidas alterações:
```    
dd bs=4M if=/caminho-do-arquivo.iso of=/dev/sdx conv=fsync oflag=direct status=progress
```

### 4. Habilite o boot
As imagens de instalação do Arch Linux não suportam _secure_ _boot_. Por isso, desative-o no BIOS|UEFI. Normalmente você consegue fazer isso pressionando F2 durante a fase POST e navegando com as setas do teclado até a opção _Secure_ _Boot_. Consulte o manual da sua placa-mãe para obter detalhes. Assim que desativá-lo salve e reinicie o computador. Selecione a inicialização pelo pendrive pressionando F12 na fase POST. Na tela de instalação do Arch selecione a primeira opção. O modo padrão é UEFI e o sistema de inicialização é o systemd-boot. Caso utilize o modo BIOS o sistema de inicialização será o [syslinux](https://wiki.archlinux.org/title/Syslinux). Para ativar o _Secure_ _Boot_ após a instalação siga estes [passos](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Disabling_Secure_Boot).

Para saber seu seu computador suporta UEFI utilize o comando abaixo. Se for exibido uma lista de diretórios sem erros o sistema será inicializado no modo UEFI. Do contário,  será inicializado no modo BIOS.
```
ls /sys/firmware/efi/efivars
```

## Installation
A primeira configuração após o boot é configurar o mapa do teclado. Caso utilize o mapa americado pule para a etapa 2. 

# Etapa 1

## 1. Mapa do teclado
O mapa de teclado padão do Arch é o americado [_US_]. No Brasil utilizamos o [_br-abnt_]. Nós usamos o [_br-abnt2_]. Para visualizar todas as opções existentes em _/usr/share/kbd/keymaps_ execute os comandos abaixo:

```
localectl list-keymaps
ls /usr/share/kbd/keymaps/**/*.map.gz | less
```

Para pesquisar um mapa específico, como o [_br-abnt2_], utilize um dos comandos abaixo:

```
localectl list-keymaps | grep br-abnt
ls /usr/share/kbd/keymaps/**/*.map.gz | grep br-abnt
```

Para definir o mapa br-abnt2 utilize:
```
loadkeys br-abnt2
```

# Etapa 2

## 1. Sincronize o repositório. 
Utilize o comando abaixo para sincronizar a lista de pacotes nos repositórios _core, extra_ e _community_. É preciso estar conectadp à internet.
```
pacman -Syy
```

Para saber se o computador está conectado à internet utilize o comando abaixo. Caso esteja será exibido seu endereço [_ip_].
```
ip -c a
```

Para se conectar à rede WI-FI utilize os seguintes comandos: 
```
iwctl
device list : # exibe os dispositivos disponíveis.
station device¹ scan : # inicia a busca por redes.
station device¹ get-networks : # lista todas as redes alcançadas.
station device* connect SSID² : conecta na rede escolhida. 
Informe a senha, caso a rede esteja protegida.
```
[¹] Device é o nome da interface de rede: wlan0, etc.; [²] SSID é o nome da rede.

## 2. Adicione um espelho
Às vezes a velocidade da conexão com a internet pode ser um problema. Para contorná-lo podemos utilizar os pacotes de instalação de espelhos de download mais próximos. Veja a lista completa [aqui](https://archlinux.org/mirrors/). Neste tutorial utilizaremos o comando abaixo para habilitar o mirror do "Brazil".
```
reflector -–verbose -c “Brazil” --sort rate >> /etc/pacman.d/mirrorlist
```

## 3. Downloads paralelos
Um recurso interessante do Pacman contra a baixa velocidade da conexão com a internet é permitir downloads em paralelo. Contudo, por padão essa configuração está desativada. Para ativá-la  edite o arquivo abaixo e descomente a linha correspondente, salve o arquivo e continue a instalação.
```
nano /etc/pacman.conf 
ParallelDownloads = 5
```

# Etapa 3

## 1. Sincronize a data e a hora.
Utilize o comando abaixo para onfigurar alterar o relógio do sistema e ativar o serviço de sincronização.
```
timedatectl set-ntp true
```

# Etapa 4

## 1. Particione o disco
Criaremos 3 partições do tipo _GPT_: [/boot, / e /HOME]. Utilize o primeiro comando _lsblk_ para visualizar as unidades e o comando _gdisk_ para iniciar o particionamento. Tenha bastante atenção ao definir o disco a ser particionado {sda, sdb, nvme, etc.}. Todos os dados serão apagados. Quando solicitado, pressione _n_ para criar uma nova partição. O tamanho mínimo sugerido para _/home_ é 10G. Quando terminar, pressione _w_ para gravar e _y_ para confirmar. Siga as etapas abaixo:
```
lsblk
gdisk /dev/disco
```
Crie as seguintes partições:
```
Partição EFI de [+300M] -> tipo [ef00]
Partição / de [+30G] -> tipo [8300]
Partição HOME de [+XG] -> tipo [8300]
```

## 2. Formate as partições
Utilize o tipo [_fat_] para a partição de [_boot_] e [_ext4_] para as demais partições. Veja os comandos abaixo:
```
mkfs.vfat /dev/disco¹
mkfs.ext4 /dev/disco²
mkfs.ext4 /dev/disco³
```
[¹] Partição boot ex.: /dev/sda1 ; [²] Partição / ; [³] Partição /home.

## 3. Monte as partições
Utilize o comando [_mount_] para montar as partições nos seguintes destinos:
```
mount /dev/disco² /mnt
mkdir -p /mnt/{boot/efi,home}
mount /dev/disco¹ /mnt/boot
mount /dev/disco³ /mnt/home
```   

[¹] Partição boot. ex.: /dev/sda1 ; [²] Partição / ; [³] Partição /home.

# Etapa 5

## 1. Baixe os pacotes principais
Utilize o comando [_pacstrap_] para baixar e instalar os pacotes [_base linux linux-firmware sudo git nano intel-ucode e reflector_] em [_/mnt_].
```
pacstrap /mnt base linux¹ linux-firmware sudo git nano reflector intel-ucode (or amd-ucode)
```

[¹] Pode optar pelo kernel linux-lts e linux-zen 


## 2. FSTAB
Utilize o comando [_genfstab_] para gerar o arquivo [_fstab_] com as partições montadas na etapa anterior.
```
genfstab -U¹ /mnt >> /mnt/etc/fstab
```
[¹] Utiliza o UUID da partição em vez do nome; 

# Etapa 6

## 1. Chroot
Chegou a hora de mudar para o [_Shell_] de dentro da instalação. Utilize o comando [_Arch-chroot_] no ponto de montagem [_/mnt_] assim:
```
arch-chroot /mnt
```

# Etapa 7

## 1. Clone o repositório
Utilize o comando [_git clone_] para copiar os _scripts_ de instalação personalizados disponíveis no nosso repositório

    git clone https://gitlab.com/zerodevsystem/z-scripts.git

## 2. Acesse o diretório z-scripts
Após o download acesse o diretório com o comando:

    cd z-scripts

## 3. Configuração inicial
O script _base-grub.sh_ realiza as primeiras configurações pós-instalação, como a _Timezone_, o idioma, o nome da máquina, instala pacotes fundamentais para a utilização do sistema, como grub, network-manager, bluez, cups, qemu, libvirt, farewall, flatpak, entre outros, habilita alguns serviços, cria o usuário e insere-o nos grupos _users_, _wheel_ e _sudoers_. Porém, antes de executá-lo propriamente, é preciso torná-lo executável. Para isso, utilize o comando [_chmod_]. Depois, execute-o assim: 
```
chmod +x base.sh
./base-grub.sh
```

# Etapa 8

## Senha do root 
Altere a senha do usuário root com o seguinte comando:
```
passwd root
```

## Contributing
Feel free to contribute. If you prefer, send your suggestions to zerodevsystem@gmail.com

## Authors and acknowledgment
Fábio Linhares | fabiolinharez@gmail.com

## License
Copyright (C) 2022 [Free Software Foundation, Inc.](https://fsf.org/). Everyone is permitted to copy and distribute verbatim copies of this project and all documents, changing it is allowed.

## Project status
Active